﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainComponentSpec
{
    public interface IKawa
    {
        int Wybierzkawe();
        void ZrobKawe();
        void Koniec();
    }
}
