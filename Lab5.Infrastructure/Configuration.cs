﻿using System;
using PK.Container;
using MainComponentImpl;
using MainComponentSpec;
using DisplayComponentImpl;
using DisplayComponentSpec;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IEkran ekran = new Ekran();
            IKawa ekspress = new Kawa();
            IContainer container = new Container();

            container.Register(ekran);
            container.Register(ekspress);

            return container;


            
        }
    }
}
