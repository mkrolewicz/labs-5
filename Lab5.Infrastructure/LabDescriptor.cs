﻿using System;
using System.Reflection;
using PK.Container;

using DisplayComponentSpec;
using DisplayComponentImpl;
using MainComponentSpec;
using MainComponentImpl;


namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IKawa));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Kawa));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IEkran));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Ekran));

        #endregion
    }
}
