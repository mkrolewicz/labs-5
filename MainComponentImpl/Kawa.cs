﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainComponentSpec;

namespace MainComponentImpl
{
    public class Kawa : IKawa
    {

        public int Wybierzkawe()
        {
            return 1;
        }

        public void ZrobKawe()
        {
            Console.WriteLine("Robie kawe");
        }

        public void Koniec()
        {
            Console.WriteLine("Zrobiłem kawe");
        }
    }
}
